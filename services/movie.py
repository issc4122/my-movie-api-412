from models.movies import Movies as MovieModel
from schemas.movie import Movie as MovieBase

class MovieService():
    def __init__(self, db) -> None:
        self.db = db
    def get_movies(self):
        result = self.db.query(MovieModel).all()
        return self
    def get_movie(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        return result
    def get_movies_by_category(self, category: str):
        result = self.db.query(MovieModel).filter(MovieModel.category == category).all()
        return result
    def create_movie(self, Movie: MovieBase):
        new_movie = MovieModel(**Movie.model_dump())
        self.db.add(new_movie)
        self.db.commit()
        return new_movie
    def delete_movies(self, id: int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).delete()
        self.db.commit()
        return
    def update_movie(self, Movie: MovieBase, id : int):
        result = self.db.query(MovieModel).filter(MovieModel.id == id).first()
        result.title = Movie.title
        result.overview = Movie.overview
        result.year = Movie.year
        result.rating = Movie.rating
        result.category = Movie.category
        self.db.commit()
        return