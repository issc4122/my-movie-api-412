from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import JSONResponse, HTMLResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.computadoras import Computer as ComputerModel
from utils.jwt_manager import create_token
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBerrer

computer_router = APIRouter()

class User(BaseModel):
    email:str
    password:str

class Computer(BaseModel):
    id: Optional[int] = None
    marca: str = Field(default = "Marca de computadora", min_length = 3, max_length=15)
    modelo: str = Field(default="Modelo de computadora", min_length = 3, max_length=50)
    color: str = Field(default = "Negro", min_length = 3, max_length = 10)
    ram: str = Field(default = "RAM", min_length = 3, max_length = 15)
    almacenamiento: str = Field(default = "Almacenamiento", min_length = 3, max_length=20)

    class Config:
        json_schema_extra = {
            "example": {
                "id" : 1,
                "marca" : "Marca de la computadora",
                "modelo" : "Modelo de computadora",
                "color" : "Color",
                "ram" : "Ram",
                "almacenamiento" : "Almacenamiento"
            }
        }

@computer_router.get('/', tags = ['home'])

@computer_router.post('/login', tags = ['auth'])
def login(user: User):
    if user.email == "victor7044@gmail.com" and user.password == "password":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
@computer_router.get('')

def message():
    return HTMLResponse('<h1>Venta de computadoras<h1>')

@computer_router.get('/computers', tags=['computers'], response_model=List[Computer], status_code=200, dependencies=[Depends(JWTBerrer())])
def get_computers() -> List[Computer]:
    db = Session()
    result = db.query(ComputerModel).all()
    return JSONResponse(status_code = 200, content=jsonable_encoder(result))

@computer_router.get('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def get_computer(id: int) -> Computer:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    return JSONResponse(status_code= 200, content=jsonable_encoder(result))

@computer_router.get('/computers/', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def get_computers_by_marca(marca: str):
   db = Session()
   result = db.query(ComputerModel).filter(ComputerModel.marca == marca).all()
   if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado computadoras con esa marca"})
   return JSONResponse(status_code= 200, content=jsonable_encoder(result))

@computer_router.post('/computers/', tags=['computers'], response_model=List[Computer],dependencies=[Depends(JWTBerrer())])
def create_computers(Computers: Computer):
    #Nueva sesión de computadoras``
    db = Session()
    #Nueva computadora con el modelo de computadoras
    new_cmp = ComputerModel(**Computers.model_dump())
    #Agregar nueva computadora
    db.add(new_cmp)
    #Commit
    db.commit()
    return JSONResponse(content= {"message" : "Se ha registrado la computadora"})

@computer_router.delete('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def delete_computers(id: int):
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code= 200, content= {"message" : "Se ha eliminado la computadora"})

@computer_router.put('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def edit_computers(id : int, comp : Computer):
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    result.marca = comp.marca
    result.modelo = comp.modelo
    result.color = comp.color
    result.almacenamiento = comp.almacenamiento
    result.ram = comp.ram
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la computadora"})

    #for item in computers:
    #    if item["id"] == id:
    #        return JSONResponse(status_code= 200, content=item)
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})



   #computadoras = []
   #for item in computers:
   #     if item["marca"] == marca:
   #         computadoras.append(item) 
   #return JSONResponse(content=computadoras)



    
    #for item in computers:
    #    if item["id"] == id:
    #        computers.remove(item)
    #        return JSONResponse(status_code= 200, content= {"message" : "Se ha eliminado la computadora"})
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
     



    #for item in computers:
    #    if id == item["id"]:
    #        item["marca"] = marca
    #        item["modelo"] = modelo 
    #        item["color"] = color
    #        item["ram"] = ram
    #        item["almacenamiento"] = almacenamiento
    #        return JSONResponse(status_code=200, content={"message": "Se ha editado la computadora"})
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})