from fastapi import APIRouter, Depends
from fastapi.responses import JSONResponse, HTMLResponse
from typing import List
from config.database import Session
from models.movies import Movies as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBerrer
from services.movie import MovieService
from schemas.movie import Movie


movie_router = APIRouter()

@movie_router.get('/', tags = ['home'])

def message():
    return HTMLResponse('<h1>Hello world<h1>')

@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBerrer())])
def get_movies():
    db = Session()
    result = MovieService(db).get_movies()
    return JSONResponse(status_code = 200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags =['movies'], response_model=List[Movie], dependencies=[Depends(JWTBerrer())])
def get_movie(id: int):
    db = Session()
    result = MovieService(db).get_movie()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la pelicula"})
    return JSONResponse(status_code= 200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBerrer())])
def get_movies_by_category(category: str):
    db = Session()
    result = MovieService(db).get_movies_by_category()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado pelicula con esa categoria"})
    return JSONResponse(status_code= 200, content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBerrer())])
def create_movie(Movies : Movie):
    db = Session()
    MovieService(db).create_movie()
    return JSONResponse(content= {"message" : "Se ha registrado la pelicula"})

@movie_router.delete('/movies/{id}', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBerrer())])
def delete_movies(id: int):
    db = Session()
    result = MovieService(db).get_movie()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la pelicula"})
    MovieService(db).delete_movies(id)
    return JSONResponse(status_code= 200, content= {"message" : "Se ha eliminado la pelicula"})

@movie_router.put('/movies/{id}', tags=['movies'], response_model=List[Movie], dependencies=[Depends(JWTBerrer())])
def edit_movies(id : int, movie : Movie):
    db = Session()
    result = MovieService(db).get_movie()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la pelicula"})
    result = MovieService(db).update_movie(Movie, id)
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la pelicula"})