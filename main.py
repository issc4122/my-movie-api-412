# id, marca, modelo, color, ram y almacenamiento
import uvicorn
from fastapi import FastAPI, Body, Path, Request, HTTPException, Depends
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from utils.jwt_manager import create_token, validate_token
from fastapi.security import HTTPBearer
from config.database import Session, engine, Base
from models.computadoras import Computer as ComputerModel
from fastapi.encoders import jsonable_encoder
from middlewares.error_handler import ErrorHandler
from middlewares.jwt_bearer import JWTBerrer

app = FastAPI()
app.title = "Mi primera chamba con APIS"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)

Base.metadata.create_all(bind=engine)

class User(BaseModel):
    email:str
    password:str
    
class JWTBerrer(HTTPBearer):
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if (data['email'] != "victor7044@gmail.com"):
            raise HTTPException(status_code=403, detail="Credenciales invalidas.")

class Computer(BaseModel):
    id: Optional[int] = None
    marca: str = Field(default = "Marca de computadora", min_length = 3, max_length=15)
    modelo: str = Field(default="Modelo de computadora", min_length = 3, max_length=50)
    color: str = Field(default = "Negro", min_length = 3, max_length = 10)
    ram: str = Field(default = "RAM", min_length = 3, max_length = 15)
    almacenamiento: str = Field(default = "Almacenamiento", min_length = 3, max_length=20)

    class Config:
        json_schema_extra = {
            "example": {
                "id" : 1,
                "marca" : "Marca de la computadora",
                "modelo" : "Modelo de computadora",
                "color" : "Color",
                "ram" : "Ram",
                "almacenamiento" : "Almacenamiento"
            }
        }

@app.get('/', tags = ['home'])

@app.post('/login', tags = ['auth'])
def login(user: User):
    if user.email == "victor7044@gmail.com" and user.password == "password":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
@app.get('')

def message():
    return HTMLResponse('<h1>Venta de computadoras<h1>')

@app.get('/computers', tags=['computers'], response_model=List[Computer], status_code=200, dependencies=[Depends(JWTBerrer())])
def get_computers() -> List[Computer]:
    db = Session()
    result = db.query(ComputerModel).all()
    return JSONResponse(status_code = 200, content=jsonable_encoder(result))

@app.get('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def get_computer(id: int) -> Computer:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    return JSONResponse(status_code= 200, content=jsonable_encoder(result))
    #for item in computers:
    #    if item["id"] == id:
    #        return JSONResponse(status_code= 200, content=item)
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})

@app.get('/computers/', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def get_computers_by_marca(marca: str):
   db = Session()
   result = db.query(ComputerModel).filter(ComputerModel.marca == marca).all()
   if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado computadoras con esa marca"})
   return JSONResponse(status_code= 200, content=jsonable_encoder(result))

   #computadoras = []
   #for item in computers:
   #     if item["marca"] == marca:
   #         computadoras.append(item) 
   #return JSONResponse(content=computadoras)


@app.post('/computers/', tags=['computers'], response_model=List[Computer],dependencies=[Depends(JWTBerrer())])
def create_computers(Computers: Computer):
    #Nueva sesión de computadoras``
    db = Session()
    #Nueva computadora con el modelo de computadoras
    new_cmp = ComputerModel(**Computers.model_dump())
    #Agregar nueva computadora
    db.add(new_cmp)
    #Commit
    db.commit()
    return JSONResponse(content= {"message" : "Se ha registrado la computadora"})

@app.delete('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def delete_computers(id: int):
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    db.delete(result)
    db.commit()
    return JSONResponse(status_code= 200, content= {"message" : "Se ha eliminado la computadora"})
    
    #for item in computers:
    #    if item["id"] == id:
    #        computers.remove(item)
    #        return JSONResponse(status_code= 200, content= {"message" : "Se ha eliminado la computadora"})
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
     

@app.put('/computers/{id}', tags=['computers'], response_model=List[Computer], dependencies=[Depends(JWTBerrer())])
def edit_computers(id : int, comp : Computer):
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})
    result.marca = comp.marca
    result.modelo = comp.modelo
    result.color = comp.color
    result.almacenamiento = comp.almacenamiento
    result.ram = comp.ram
    db.commit()
    return JSONResponse(status_code=200, content={"message": "Se ha modificado la computadora"})

    #for item in computers:
    #    if id == item["id"]:
    #        item["marca"] = marca
    #        item["modelo"] = modelo 
    #        item["color"] = color
    #        item["ram"] = ram
    #        item["almacenamiento"] = almacenamiento
    #        return JSONResponse(status_code=200, content={"message": "Se ha editado la computadora"})
    #return JSONResponse(status_code= 404, content= {"message" : "No se ha encontrado la computadora"})