from fastapi import FastAPI
from config.database import engine, Base
from routes.movie import movie_router
from routes.User import user_router
from middlewares.error_handler import ErrorHandler

app = FastAPI()
app.title = "Tienda de peliculas"

app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)